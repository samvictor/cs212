// FILE: point.cpp
// CLASS IMPLEMENTED: point (See point.h for documentation.)

#include "point.h"

namespace main_savitch_2A
{
    
    point::point(double initial_x, double initial_y)
    {
        m_x = initial_x;   // Constructor sets the point to a given position.
        m_y = initial_y;
    }

    
    void point::shift(double x_amount, double y_amount)
    {
        m_x += x_amount;
        m_y += y_amount;   
    }

    
    void point::rotate90( )
    {
        double new_x;
        double new_y;

        new_x =  m_y;  // For a 90 degree clockwise rotation, the new x is the original y,
        new_y = -m_x;  // and the new y is -1 times the original x.
        m_x = new_x;
        m_y = new_y; 
    }
}
