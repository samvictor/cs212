// FILE: newpoint.cpp
// CLASS IMPLEMENTED: point (See newpoint.h for documentation.)

#include <iostream>
#include <math.h>
#include "newpoint.h"
using namespace std;

namespace main_savitch_2B
{
    point::point(double initial_x, double initial_y)
    {
	m_x = initial_x;   // Constructor sets point to a given position
	m_y = initial_y;
    }

    void point::shift(double x_amount, double y_amount)
    {
	m_x += x_amount;
	m_y += y_amount;   
    }

    void point::rotate90( )
    {
	double new_x;
	double new_y;

	new_x =  m_y;  // For a 90 degree clockwise rotation the new y is -1
	new_y = -m_x;  // times original x, and the new x is the original y
	m_x = new_x;
	m_y = new_y;   
    }


    int rotations_needed(point p)
    {
	int answer;
	
	answer = 0;
	while ((p.x( ) < 0) || (p.y( ) < 0))
        {
	    p.rotate90( );
	    ++answer;
	}
	return answer;
    }

    void rotate_to_upper_right(point& p)
    {
	while ((p.x( ) < 0) || (p.y( ) < 0))
	    p.rotate90( );
    }

    double distance(const point& p1, const point& p2)
    // Library facilities used: cmath 
    {
	double a, b, c_squared;

	// Calculate differences in x and y coordinates
	a = p1.x( ) - p2.x( ); // Difference in x coordinates
	b = p1.y( ) - p2.y( ); // Difference in y coordinates
	
	// Pythagorean Theorem to calculate square of distance between points
	c_squared = a*a + b*b;

	return sqrt(c_squared); // sqrt calculates square root (from math.h)
    }

    point middle(const point& p1, const point& p2)
    {
	double x_midpoint, y_midpoint;

	// Compute the x and y midpoints
	x_midpoint = (p1.x( ) + p2.x( )) / 2;
	y_midpoint = (p1.y( ) + p2.y( )) / 2;
	
	// Construct a new point and return it
	point midpoint(x_midpoint, y_midpoint);
	return midpoint;
    }

	// DO the same addition using both memember function and nonmember function

	point point::operator^(const point& p2) const
	{
		double x_sum, y_sum;
		x_sum = (m_x + p2.x());
		y_sum = (m_y + p2.y());
		point sum(x_sum, y_sum);
		return sum;
	}

    point operator +(const point& p1, const point& p2)
    {
	double x_sum, y_sum;
	
	// Compute the x and y of the sum
	x_sum = (p1.x( ) + p2.x( ));
	y_sum = (p1.y( ) + p2.y( ));
	point sum(x_sum, y_sum);
	return sum;
    }


    bool operator ==(const point& p1, const point& p2)
    {
	return ((p1.x( ) == p2.x( )) && (p1.y( ) == p2.y( )));
    }

    bool operator !=(const point& p1, const point& p2)
    {
	return !(p1 == p2);
    }


    ostream& operator <<(ostream& outs, const point& source)
    // Library facilities used: iostream
    {
	outs << "(" << source.x( ) <<  ", "  << source.y( ) << ")";
	return outs;
    }

/* friend function implementation

    istream& operator >>(istream& ins, point& target)
    // Library facilities used: iostream
    // Friend of: point class
    {
	ins >> target.m_x >> target.m_y;
	return ins;
    }

*/

    istream& point::operator >>(istream& ins)
    // Library facilities used: iostream
    // Friend of: point class
    {
	ins >> m_x >> m_y;
	return ins;
    }


/* non-memeber function, using other member functions
    istream& operator >>(istream& ins, point& target)
    // Library facilities used: iostream
    // Friend of: point class
    {
	ins >> target.set_x() >> target.set_y();
	return ins;
    }
*/

/* non-memeber function, using constructor
    istream& operator >>(istream& ins, point& target)
    // Library facilities used: iostream
    // Friend of: point class
    {
	double xx, yy;
	ins >> xx >> yy;
	point p(xx,yy);
	target = p;
	return ins;
    }
*/

/* member function implementation

    istream& point::operator >>(istream& ins)
    // Library facilities used: iostream
    // Friend of: point class
    {
	ins >> m_x >> m_y;
	return ins;
    }
*/

}
    
