// FILE: line.h 
// CLASS PROVIDED: line (an ADT for a non-vertical line on a two-dimensional plane)
//
// CONSTRUCTORS for the line class:
//   line(double initial_slope = 0.0, double initial_intercept = 0.0)
//     Postcondition: The line has been set to (initial_slope, initial_intercept).
//   line(const point p1, const point p2)
//	 	Precondition:  p1 and p2 have different x coordinates, so do not form a vertical line
//		Postcondition:  Slope and Intercept is calculated and set based on these points
//						assertion error if precondition is violated.

// MODIFICATION MEMBER FUNCTIONS for the point class:
//   void set_slope(const double new_slope)
//     Postcondition: The line's slope is set to new_slope.
//
//   void set_intercept(const double new_intercept)
//     Postcondition: The line's intercept is set to new_intercept.
//
// CONSTANT MEMBER FUNCTIONS for the line class:
//   double slope( ) const
//     Postcondition: The value returned is the line's slope.
//
//   double intercept( ) const
//     Postcondition: The value returned is the line's y-intercept.
//
//   bool contains(const point p) const
//	    Postcondition:  True if the point p lies on this line.
//	 bool contains(const point points[], const size_t length) const
//		Precondition:  length is the size of points[]
//		Postcondition:  True if every point in points[] lies on this line.
//
// NONMEMBER FUNCTIONS for the line class:
//   line operator +(const line& l1, const line& l2)
//     Postcondition: The sum of l1 and l2 is returned.
//
//   bool operator ==(const line& l1, const line& l2)
//     Postcondition: The return value is true if l1 and l2 are identical.
//
//   bool operator !=(const line& l1, const line& l2)
//     Postcondition: The return value is true if l1 and l2 are not identical.
//
//   ostream& operator <<(ostream& outs, const line& source)
//     Postcondition: The slope and intercept of source have been 
//     written to outs. The return value is the ostream outs.
//
//   istream& operator >>(istream& ins, line& target)
//     Postcondition: The slope and intercept of target have been 
//     read from ins. The return value is the istream ins.
//
//   bool parallel(const line& l1, const line& l2)
//     Postcondition: True returned if l1 and l2 are parallel (have the same slope).
//
//   bool intersect(const line& l1, const line& l2)
//     Postcondition: True returned if l1 and l2 intercect (have the different slope or represent same line)
//
//   point point_of_intersection(const line& l1, const line& l2)
//     	PreCondition:  l1 and l2 intersect
//		Postcondition: returns point of intersection, or if lines are identical, returns point at y-intercept
//
// VALUE SEMANTICS for the line class:
//   Assignments and the copy constructor may be used with line objects.

#ifndef MAIN_SAVITCH_LINE_H   
#define MAIN_SAVITCH_LINE_H  
#include <iostream> // Provides ostream and istream
#include "newpoint.h"

namespace main_savitch_2B
{          
    class line
    {
    public:
        // CONSTRUCTOR
        line(double initial_slope = 0.0, double initial_intercept = 0.0);
        line(const point p1, const point p2);
		// MODIFICATION MEMBER FUNCTIONS
        void set_slope(const double new_slope);
        void set_intercept(const double new_intercept);

		std::istream& operator>>(std::istream& ins);

        // CONSTANT MEMBER FUNCTIONS
        double slope( ) const { return m_slope; } 
        double intercept( ) const { return m_intercept; }
		bool contains(const point p) const;
		bool contains(const point p[], const size_t length) const;
       
	   // FRIEND FUNCTION
	    friend bool parallel(const line l1, const line l2);
		friend bool intersect(const line l1, const line l2);
		friend point point_of_intersection(line const l1, const line l2);
		
    private:
        double m_slope, m_intercept; // x and y coordinates of this point
    };

    // NONMEMBER FUNCTIONS for the point class 
    line operator +(const line& l1, const line& l2);
    bool operator ==(const line& l1, const line& l2);
    bool operator !=(const line& l1, const line& l2);
    std::ostream& operator <<(std::ostream & outs, const line& source);
	
}

#endif 
