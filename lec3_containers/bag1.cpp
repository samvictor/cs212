// FILE: bag1.cpp
// CLASS IMPLEMENTED: bag (see bag1.h for documentation)
// INVARIANT for the bag class:
//   1. The number of items in the bag is in the member variable m_used;
//   2. For an empty bag, we do not care what is stored in any of m_data; for a
//      non-empty bag the items in the bag are stored in m_data[0] through
//      m_data[m_used-1], and we don't care what's in the rest of m_data.

#include <algorithm> // Provides copy function
#include <cassert>   // Provides assert function
#include "bag1.h"
using namespace std;

namespace main_savitch_3
{
    //const bag::size_type bag::CAPACITY; //already changed to enum {CAPACITY = 30}
    
    bag::size_type bag::erase(const value_type& target)
    {
	size_type index = 0;
	size_type many_removed = 0;

	while (index < m_used)
	{
	    if (m_data[index] == target)
	    {
		--m_used;
		m_data[index] = m_data[m_used];
		++many_removed;
	    }
	    else
		++index;
	}

	return many_removed;
    }

    bool bag::erase_one(const value_type& target)
    {
	size_type index; // The location of target in the m_data array    

	// First, set index to the location of target in the m_data array,
	// which could be as small as 0 or as large as m_used-1. If target is not
	// in the array, then index will be set equal to m_used.
	index = 0; 
	while ((index < m_used) && (m_data[index] != target))
	    ++index;

	if (index == m_used)
	    return false; // target isn�t in the bag, so no work to do.

	// When execution reaches here, target is in the bag at m_data[index].
	// So, reduce m_used by 1 and copy the last item onto m_data[index].
	--m_used;
	m_data[index] = m_data[m_used];    
	return true;
    }

	void bag::insert(const value_type& entry)
    // Library facilities used: cassert
    {   
        assert(size( ) < CAPACITY);
		m_data[m_used] = entry;
	++m_used;
    }

    void bag::operator +=(const bag& addend)
    // Library facilities used: algorithm, cassert
    {
	assert(size( ) + addend.size( ) <= CAPACITY);
	
	copy(addend.m_data, addend.m_data + addend.m_used, m_data + m_used);
	m_used += addend.m_used;
    }

    bag::size_type bag::count(const value_type& target) const
    {
        size_type answer;
        size_type i;

        answer = 0;
        for (i = 0; i < m_used; ++i)
            if (target == m_data[i])
                ++answer;
        return answer;
    }

    bag operator +(const bag& b1, const bag& b2)
    // Library facilities used: cassert
    {
        bag answer;

        assert(b1.size( ) + b2.size( ) <= bag::CAPACITY);

        answer += b1; 
        answer += b2;
        return answer;
    }
}
